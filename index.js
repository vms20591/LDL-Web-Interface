$(() => {
    $('#owncloud').click(() => {
      window.open('http://localhost/owncloud', '_blank');
    });
    $('#nextcloud').click(() => {
      window.open('http://localhost', '_blank');
    });
    $('#calibre').click(() => {
      window.open('http://localhost:8000', '_blank');
    });
    $('#kiwix').click(() => {
      window.open('http://localhost:9000', '_blank');
    });
  });
  